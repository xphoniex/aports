# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-dask
pkgver=2.15.0
pkgrel=0
pkgdesc="Parallel computing with task scheduling"
url="https://dask.org/"
arch="noarch !mips !mips64 !s390x" # Blocked by py3-partd
license="BSD-3-Clause"
depends="python3 py3-toolz py3-numpy py3-pandas py3-fsspec py3-partd py3-arrow"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-runner"
source="https://pypi.python.org/packages/source/d/dask/dask-$pkgver.tar.gz"
builddir="$srcdir/dask-$pkgver"

case "$CARCH" in
	# Python segfaults while running the tests
	ppc64le) options="$options !check" ;;
esac

build() {
	python3 setup.py build
}

check() {
	# Some test in test_shuffle.py hangs
	pytest --ignore=dask/dataframe/tests/test_shuffle.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="602811c2ee9d3ec16baaba2b1b786edac893897c68af84376d05bc34a9b529ce8b30941fdf3afba5635c0f35556e1bfcabb2c419c03a11471dbed5469a2784b6  dask-2.15.0.tar.gz"
