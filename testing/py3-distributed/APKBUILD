# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-distributed
pkgver=2.15.2
pkgrel=0
pkgdesc="A library for distributed computation"
url="https://distributed.dask.org/"
arch="noarch !mips !mips64 !s390x" # Blocked by py3-dask
license="BSD-3-Clause"
depends="python3"
depends="py3-dask py3-yaml py3-tornado py3-tblib py3-click py3-cloudpickle py3-msgpack py3-psutil py3-sortedcontainers py3-zict"
makedepends="py3-setuptools python3-dev linux-headers yaml-dev"
checkdepends="py3-pytest py3-pytest-asyncio py3-requests py3-paramiko py3-arrow py3-h5py py3-scipy py3-lz4 py3-jsonschema"
source="https://pypi.python.org/packages/source/d/distributed/distributed-$pkgver.tar.gz"
# Tests are flaky
# Net required for tests
options="!check"
builddir="$srcdir/distributed-$pkgver"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="e68658d090a5fd29f127e1e43619b4531f9d04f69bcf288c02aea1fa7f7f5427b91d9c42ee9d01107f5c9d5f8807cb81e79425ba26ced5ac4fde7976414f5531  distributed-2.15.2.tar.gz"
